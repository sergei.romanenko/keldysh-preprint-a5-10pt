#import "keldysh-preprint-a5-10pt_en.typ": keldysh_preprint_en

#show: keldysh_preprint_en.with(
  title_en:
    [MRSC: a toolkit for building\
     _multi-result_ supercompilers\
     $E = m c^2$],
  title_ru:
    [MRSC: инструментарий для создания\
     _многорезультатных_ суперкомпиляторов],
  authors_en: (
    "Ilya G. Klyuchnikov",
    "Sergei A. Romanenko",
    "Unknown X. Person",
    "Unknown X. Person",
  ),
  authors_ru: (
    "И.Г. Ключников",
    "С.А. Романенко",
  ),
  year: 2011,
  abstract_en: [
The paper explains the principles of multi-result supercompilation. We introduce a formalism for representing supercompilation algorithms as rewriting rules for graphs of configurations. Some low-level technical details related to the implementation of multi-result supercompilation in MRSC are discussed. In particular, we consider the advantages of using spaghetti stacks for representing graphs of configurations.
  ],
  abstract_ru: [
В работе рассматриваются принципы многорезультатной суперкомпиляции. Вводится формализм для представления алгоритмов суперкомпиляции в виде правил переписываний графа конфигураций. Разбираются технические детали поддержки многорезультатной суперкомпиляции в инструментарии MRSC. Особое внимание уделяется представлению графов конфигураций в виде спагетти-стеков.
  ],
  keywords_en: (
    "supercompilation",
    "program analysis",
    "program transformation",
    "domain-specific supercompilation",
  ),
  keywords_ru: (
    "суперкомпиляция",
    "анализ программ",
    "преобразование программ",
    "предметно-ориентированная суперкомпиляция",
    "многорезультатная суперкомпиляция",
  ),
  supported_by_en: [Supported by Russian Foundation for Basic Research project
No.~99-99-99999-a.],
  supported_by_ru: [Работа выполнена при поддержке проекта РФФИ
№~99-99-99999-a.],
  outline_after_annotations: true,
)

= Introduction <section:introduction>

== Growing variety in the field of supercompilation <section:growing-variety>

_Supercompilation_ is a program manipulation technique that was originally introduced by V. Turchin in terms of the programming language Refal (a first-order applicative functional language) @Turchin1986Supercompiler, for which reason the first supercompilers were designed and developed for the language Refal @Turchin1979ScpSystem@Turchin1982Experiments@Nemytykh1996ProgTrans.

= Schemes of traditional supercompilation <section:supercompilation_schemes>

In the supercompilation community, there are two well-established approaches to describing and implementing supercompilers.

The first approach formulates supercompilation in terms of the construction of a graph of configurations that is then transformed (residuated) into an output (residual) program @Turchin1979ScpSystem@Sorensen1994TurchinSupercompiler@Sorensen1996Positive@Klyuchnikov2009SPSC@Klyuchnikov2009HOSC@Hamilton2010Graph. The origin of this approach goes back to V. Turchin @Turchin1986Supercompiler.

The second approach considers a supercompiler as an expression transformer that produces output programs "directly", avoiding the construction of intermediate data structures (graphs of configurations)#footnote[At least in an explicit way.];.

= Conclusion <section:conclusion>

This preprint describes only the internal structure and technical design of the MRSC core. Next preprints will present concrete examples of rapid prototyping of supercompilers by means of MRSC and the use of MRSC for implementing domain-specific supercompilers.

#heading(level: 1, numbering: none, outlined: false)[Acknowledgements]
<acknowledgements>
The authors express their gratitude to all participants of Refal seminar at Keldysh Institute for useful comments and fruitful discussions of this work.

#bibliography("refs.bib")
// #bibliography("refs.yml")
