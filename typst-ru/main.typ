#import "keldysh-preprint-a5-10pt_ru.typ": keldysh_preprint_ru

#show: keldysh_preprint_ru.with(
  title_ru:
    [MRSC: инструментарий для создания\
     _многорезультатных_ суперкомпиляторов\
     $E = m c ^2$],
  title_en:
    [MRSC: a toolkit for building\
     _multi-result_ supercompilers],
  authors_ru: (
    "И.Г. Ключников",
    "С.А. Романенко",
    "И.И. Пупкин",
    "И.И. Пупкин",
  ),
  authors_en: (
    "Ilya G. Klyuchnikov",
    "Sergei A. Romanenko",
    ),
  year: 2011,
  abstract_en: [
The paper explains the principles of multi-result supercompilation. We introduce a formalism for representing supercompilation algorithms as rewriting rules for graphs of configurations. Some low-level technical details related to the implementation of multi-result supercompilation in MRSC are discussed. In particular, we consider the advantages of using spaghetti stacks for representing graphs of configurations.
  ],
  abstract_ru: [
В работе рассматриваются принципы многорезультатной суперкомпиляции. Вводится формализм для представления алгоритмов суперкомпиляции в виде правил переписываний графа конфигураций. Разбираются технические детали поддержки многорезультатной суперкомпиляции в инструментарии MRSC. Особое внимание уделяется представлению графов конфигураций в виде спагетти-стеков.
  ],
  keywords_en: (
    "supercompilation",
    "program analysis",
    "program transformation",
    "domain-specific supercompilation",
  ),
  keywords_ru: (
    "суперкомпиляция",
    "анализ программ",
    "преобразование программ",
    "предметно-ориентированная суперкомпиляция",
    "многорезультатная суперкомпиляция",
  ),
  supported_by_en: [Supported by Russian Foundation for Basic Research project
No.~99-99-99999-a.],
  supported_by_ru: [Работа выполнена при поддержке проекта РФФИ
№~99-99-99999-a.],
  outline_after_annotations: true,
)

= Введение <section:introduction>

== Нарастающее многообразие в области суперкомпиляции <section:growing_variety>

_Суперкомпиляция_ – это метод преобразования программ, первоначально разработанный В.Ф. Турчиным для языка программирования Рефал (функциональный язык первого порядка с вызовом по имени) @Turchin1986Supercompiler, и первые суперкомпиляторы разрабатывались и реализовывались для языка Рефал @Turchin1979ScpSystem@Turchin1982Experiments@Nemytykh1996ProgTrans.

= Схемы традиционной суперкомпиляции <section:supercompilation_schemes>

В настоящее время наибольшей популярностью пользуются два подхода к методам описания и реализации суперкомпиляторов.

В соответствии с первым подходом, суперкомпиляция выполняется путем построения графа конфигураций, который затем "резидуализируется" – превращается в "остаточную" программу, которая и является окончательным результатом работы суперкомпилятора @Turchin1979ScpSystem@Sorensen1994TurchinSupercompiler@Sorensen1996Positive@Klyuchnikov2009SPSC@Klyuchnikov2009HOSC@Hamilton2010Graph. Этот подход соответствует первоначальному описанию суперкомпиляции, данному В.Ф. Турчиным @Turchin1986Supercompiler.

В соответствии со вторым подходом, суперкомпилятор – это преобразователь программ, который порождает выходные программы "напрямую", не создавая промежуточные структуры данных (графы конфигураций)#footnote[По крайней мере, не делает это в явном виде];.

= Заключение <section:conclusion>

В данном препринте описана только внутренняя структура и самый нижний, "технический" этаж инструментария MRSC. В следующих препринтах мы рассмотрим конкретные примеры, показывающие, как можно использовать MRSC для быстрого прототипирования суперкомпиляторов, а также для реализации предметно-ориентированных суперкомпиляторов.

#heading(level: 1, numbering: none, outlined: false)[Благодарности]
<section:acknowledgements>

Авторы выражает признательность участникам Рефал-семинаров, проводимых в ИПМ им. М.В. Келдыша, за ценные замечания и плодотворные обсуждения этой работы, а также Наташе и Лене за их любовь и терпение.

#bibliography("refs.bib", style: "gost-r-705-2008-numeric")
// #bibliography("refs.yml", style: "gost-r-705-2008-numeric")
