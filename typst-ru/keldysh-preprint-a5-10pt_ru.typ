#let keldysh_preprint_ru(
  title_ru: [],
  title_en: [],
  authors_ru: (),
  authors_en: (),
  year: 2100,
  abstract_en: [],
  abstract_ru: [],
  keywords_en: (),
  keywords_ru: (),
  supported_by_en: [],
  supported_by_ru: [],
  outline_after_annotations: false,
  outline_before_body: false,
  outline_after_body: false,

  doc,
) = {
  set document(
    title: title_ru,
    author: authors_ru.join(", "),
    keywords: keywords_ru.join(", "),
  )

  set page(
    "a5",
    margin: 15mm,
    // margin: (top: 15mm, bottom: 15mm, left: 15mm, right: 15mm),
    numbering: "1",
    number-align: center + top,
    header: context {
      let n = counter(page).get().first()
      if n > 2 [ #h(1fr) #n  #h(1fr) ]},
  )

  align(center)[
    *Ордена Ленина*\
    *ИНСТИТУТ ПРИКЛАДНОЙ МАТЕМАТИКИ*\
    *имени М.В.Келдыша* \
    *Российской академии наук*

    #v(6fr)

    *#box(width: 80%, { authors_ru.map(box).join(", ") })*

    #v(3mm)

    *#title_ru*

    #v(10fr)

    *Москва* \ *#year*
  ]
  pagebreak()

  set par(justify: true)
  set text(hyphenate: true)

  let rm_lb(c) = c.children.filter(it => it != linebreak()).join()

  set text(lang: "en")
  block[
    *#authors_en.join(", "). #rm_lb(title_en)*
    #v(-0.2em)
    #abstract_en

    *Keywords:* #keywords_en.join(", ").

    #supported_by_en
  ]
  
  v(1.2em)
  set text(lang: "ru")
  block[
    *#authors_ru.join(", "). #rm_lb(title_ru)*
    #v(-0.2em)
    #abstract_ru

    *Ключевые слова:* #keywords_ru.join(", ").

    #supported_by_ru
  ]

  set text(lang: "ru")
  set heading(numbering: "1.1")

  set par(first-line-indent: 1.3em, spacing: 0.65em)

  // Inserting a fake paragraph to make indented
  // the first line after a heading.

  show heading: it => {
    it
    let a = par(box())
    a
    v(-0.3 * measure(2 * a).width)
  }

  let insert_outline() = outline(depth: 2, indent: 1em)

  if outline_after_annotations { v(2fr); insert_outline(); v(1fr) }
  pagebreak()

  if outline_before_body { insert_outline() }
  doc
  if outline_after_body { pagebreak(); insert_outline() }
}
